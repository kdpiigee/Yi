﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Framework.Infrastructure.Enums
{
    public enum OrderByEnum
    {
        Asc,
        Desc
    }
}
