﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Framework.Infrastructure.Enums
{
    /// <summary>
    /// 定义公共文件路径
    /// </summary>
    public enum FileTypeEnum
    {
        File,
        Image,
        Thumbnail,
        Excel,
        Temp
    }
}
