﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Framework.Infrastructure.Data.Entities
{
    public interface IOrderNum
    {
        int OrderNum { get; set; }
    }
}
