﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Framework.Infrastructure.Const
{
    /// <summary>
    /// 定义公共文件常量
    /// </summary>
    public class PathConst
    {
        public const string wwwroot = "wwwroot";
        public const string DataTemplate = "_DataTemplate";
        public const string DataExport = "_DataExport";
    }
}
