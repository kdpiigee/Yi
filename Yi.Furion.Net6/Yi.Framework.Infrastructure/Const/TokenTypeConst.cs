﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Framework.Infrastructure.Const
{
    public class TokenTypeConst
    {
        public const string Id = nameof(Id);

        public const string UserName = nameof(UserName);

        public const string TenantId = nameof(TenantId);

        public const string DeptId= nameof(DeptId);

        public const string Email = nameof(Email);

        public const string PhoneNumber = nameof(PhoneNumber);

        public const string Roles = nameof(Roles);

        public const string Permission = nameof(Permission);
    }
}
