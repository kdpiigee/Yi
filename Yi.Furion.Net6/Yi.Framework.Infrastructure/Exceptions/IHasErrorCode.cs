﻿namespace Yi.Framework.Infrastructure.Exceptions
{
    internal interface IHasErrorCode
    {
        int Code { get; }
    }
}
