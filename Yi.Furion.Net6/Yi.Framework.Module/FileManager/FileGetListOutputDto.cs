﻿using Yi.Framework.Infrastructure.Ddd.Dtos.Abstract;

namespace Yi.Framework.Module.FileManager
{
    public class FileGetListOutputDto : IEntityDto
    {
        public long Id { get; set; }
    }
}
