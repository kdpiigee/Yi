﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Furion.Core.Rbac.Dtos.MonitorCache
{
    public class MonitorCacheNameGetListOutputDto
    {
        public string CacheName { get; set; }
        public string? Remark { get; set; }
    }
}
