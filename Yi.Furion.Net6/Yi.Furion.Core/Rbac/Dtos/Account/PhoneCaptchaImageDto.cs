﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Furion.Core.Rbac.Dtos.Account
{
    public class PhoneCaptchaImageDto
    {
        public string Phone { get; set; }
    }
}
