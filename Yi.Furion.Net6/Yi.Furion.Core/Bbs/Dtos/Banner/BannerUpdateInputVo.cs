namespace Yi.Furion.Core.Bbs.Dtos.Banner
{
    public class BannerUpdateInputVo
    {
        public string? Name { get; set; }
        public string? Logo { get; set; }
        public string? Color { get; set; }
    }
}
