using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Furion.Core.Bbs.Dtos.Banner
{
    /// <summary>
    /// Banner输入创建对象
    /// </summary>
    public class BannerCreateInputVo
    {
        public string Name { get; set; }
        public string? Logo { get; set; }
        public string? Color { get; set; }
    }
}
