using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Furion.Core.Bbs.Consts
{
    /// <summary>
    /// 常量定义
    /// </summary>

    public class DiscussConst
    {
        public const string 主题不存在 = "传入的主题id不存在";

        public const string 私密 = "【私密】您无该主题权限，可联系作者申请开放";
    }
}
