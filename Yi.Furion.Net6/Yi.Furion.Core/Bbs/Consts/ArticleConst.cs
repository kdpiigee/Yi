using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Furion.Core.Bbs.Consts
{
    /// <summary>
    /// 常量定义
    /// </summary>

    public class ArticleConst
    {
        public const string 文章不存在 = "传入的文章id不存在";

        public const string 文章无权限 = "该文章无权限";
    }
}
